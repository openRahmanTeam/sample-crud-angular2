import { PaoBKFrontendPage } from './app.po';

describe('pao-bk-frontend App', () => {
  let page: PaoBKFrontendPage;

  beforeEach(() => {
    page = new PaoBKFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
