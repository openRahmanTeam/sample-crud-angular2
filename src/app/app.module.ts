import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AccordionModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';

import { AboutComponent } from './about/about.component';
import { IndexComponent } from './index/index.component';
import { ContentModule } from './content/content.module';
import { TransactionModule } from './transaction/transaction.module';

import{ ProgressIndicatorService } from './progress-indicator.service';
import { PagerService } from './pager.service';

const routingApplication: Routes = [
  { path: "about", component: AboutComponent },
  { path: "content", redirectTo:"/content", pathMatch: "full" },
  { path: "**", component: IndexComponent },  
];
@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    NavigationbarComponent,
    AboutComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routingApplication),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    ContentModule,
    TransactionModule
  ],
  providers: [ProgressIndicatorService, PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
