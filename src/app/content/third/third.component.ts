import { Component, OnInit } from '@angular/core';

import { ProgressIndicatorService } from '../../progress-indicator.service';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {

  constructor(private progress : ProgressIndicatorService) { }

  ngOnInit() {
  }

  showNotification(){
    this.progress.toggleIndicator("Halo");

    setTimeout(() => this.progress.toggleIndicator(null), 3000);
  }

hideNotification(){
    this.progress.toggleIndicator(null);
}

}
