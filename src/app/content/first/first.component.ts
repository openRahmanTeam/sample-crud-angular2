import { Component, OnInit } from '@angular/core';
import { Router }   from '@angular/router';

import { Content } from '../content.model';

import { ContentService } from '../content.service';
import { SaveStudentService } from './save-student.service';
import { ProgressIndicatorService } from '../../progress-indicator.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  listContent: Content[];
  log(val) { console.log(val); }

  constructor(private contentService: ContentService, 
    private saveStudentService: SaveStudentService,
    private router: Router,
    private progress: ProgressIndicatorService) {  
    contentService.getContent()
      .then(result => this.listContent = result)
      .catch(this.handleError);
  }

  private handleError(errors: any): void {
    console.log("Terjadi error : "+errors);
  }

  ngOnInit() {
  }

  //////save data/////////////////
  student = new Content(undefined, undefined, 0, undefined);

  loadEditData(s : Content): void {
    this.student = new Content(s.id, s.name, s.year, s.address); 
  }

  saveOrUpdateStudent(s : Content): void{
    //this.student = new Content(s.id, s.name, s.year, s.address);   
    this.progress.toggleIndicator("process update data");
    this.saveStudentService.updateStudent(this.student, s.id)
      .then(() => {
        this.progress.toggleIndicator(null);
      })
      .catch(error => {
        console.log('failed save data student '+error);
        this.progress.toggleIndicator(null);
      }) 
  }

  deleteStudent(s : Content): void {
    this.progress.toggleIndicator("process to delete");
    this.saveStudentService.deleteStudent(s.id)
      .then(() => {
        this.deleteNotification();
        this.progress.toggleIndicator(null);
      })
      .catch(error => {
        console.log('failed delete data '+error);
        this.progress.toggleIndicator(null);
      })
  }

  deleteNotification(){
    this.progress.toggleIndicator("Data has been deleted");
    setTimeout(() => this.progress.toggleIndicator(null), 3000);
  }


  get debugForm(){
    return JSON.stringify(this.student);
  }

}
