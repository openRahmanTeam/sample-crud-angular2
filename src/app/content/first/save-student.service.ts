import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Content } from '../content.model';

@Injectable()
export class SaveStudentService {

  private serverUrl = 'api/student';
  
  constructor(private httpClient : Http) { }

  saveStudent(student : Content) : Promise<void>{
    return this.httpClient.post(this.serverUrl, student)
      .toPromise()
      .then(() => { console.log("Sukses menyimpan data"); })
      .catch(error => console.log("Error : "+error));
  }

  updateStudent(student: Content, id): Promise<void> {
    return this.httpClient.put(this.serverUrl+'/'+id, student)
      .toPromise()
      .then(() => {console.log("sukses update data")})
      .catch(error => console.log("Error : "+error));
  }

  deleteStudent(id): Promise<void> {
    return this.httpClient.delete(this.serverUrl+'/'+id)
      .toPromise()
      .then(() => {console.log("success delete data")})
      .catch(error => console.log("Error : "+error));
  }

}
