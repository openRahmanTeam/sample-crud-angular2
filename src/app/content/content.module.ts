import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { ThirdComponent } from './third/third.component';

import { ContentService } from './content.service';
import { SaveStudentService } from './first/save-student.service';
import { SamplePagingComponent } from './sample-paging/sample-paging.component';

const routingContent: Routes = [
  { path:"content/first", component: FirstComponent },
  { path:"content/second", component: SecondComponent },
  { path:"content/third", component: ThirdComponent },
  { path:"content/sample-paging", component: SamplePagingComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routingContent),
    HttpModule
  ],
  declarations: [FirstComponent, SecondComponent, ThirdComponent, SamplePagingComponent],
  exports: [SecondComponent],
  providers: [ContentService, SaveStudentService]
})
export class ContentModule { }
