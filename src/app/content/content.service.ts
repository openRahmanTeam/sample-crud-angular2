import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';

import { Content } from './content.model';

@Injectable()
export class ContentService {

  private serverUrl = 'api/student';

  constructor(private http: Http) { 
    // var obj;
    // this.getJson().subscribe(data => obj=data, error => console.log(error));
  }

  getContent(): Promise<Content[]>{
    return this.http.get(this.serverUrl)
      .toPromise()
      .then(hasil => hasil.json() as Content[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
