import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Router }   from '@angular/router';

import { Content } from '../content.model';
import { ContentService } from '../content.service';
import { ProgressIndicatorService } from '../../progress-indicator.service';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  listContent : Content[];
  @Output() selector = new EventEmitter<Content>();

  constructor(private contentService: ContentService,
              private router: Router,
              private progress: ProgressIndicatorService) { 
    contentService.getContent()
    .then(result => this.listContent = result)
    .catch(this.handleError);
  }

  private handleError(errors: any): void {
    console.log("Terjadi error : "+errors);
  }

  selectSecond(r : Content){
    this.selector.emit(r);
    console.log("selected content is "+r.name);
  }

  ngOnInit() {
  }

}
