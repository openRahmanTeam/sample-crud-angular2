import { Component, OnInit } from '@angular/core';
import { Content } from "../../content/content.model";

@Component({
  selector: 'app-second-detail',
  templateUrl: './second-detail.component.html',
  styleUrls: ['./second-detail.component.css']
})
export class SecondDetailComponent implements OnInit {

  contentSelector: Content;

  constructor() { }

  selectorSecond(con : Content){
    this.contentSelector = con;
  }

  ngOnInit() {
  }

}
