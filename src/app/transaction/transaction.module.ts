import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { SecondDetailComponent } from '../transaction/second-detail/second-detail.component';
import { ContentModule } from '../content/content.module';
import { ContentDetailComponent } from './content-detail/content-detail.component';

const routingTransaction: Routes = [
  { path:"transaction/second-detail", component:SecondDetailComponent }  
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routingTransaction),
    HttpModule,
    ContentModule
  ],
  declarations: [SecondDetailComponent, ContentDetailComponent]
})
export class TransactionModule { }
